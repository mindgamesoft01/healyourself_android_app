package healingvisualization.androidhive.cardview;

/**
 * Created by mindgame on 3/11/2017.
 */

public class AlbumObject {

    String title,des;

    public AlbumObject( String title, String des) {

        this.setTitle(title);
        this.setDes(des);
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public String getTitle() {
        return title;

    }

    public String getDes() {
        return des;
    }
}
