package healingvisualization.androidhive.cardview;

import org.json.JSONObject;

/**
 * Created by mindgame on 3/14/2017.
 */

public class albumitem
{
    String album_id;
    String album_name;
    String album_image;
    JSONObject album_songs;

    public albumitem(){
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public String getAlbum_id() {
        return album_id;
    }

    public void setAlbum_id(String album_id) {
        this.album_id = album_id;
    }

    public String getAlbum_name() {
        return album_name;
    }

    public void setAlbum_name(String album_name) {
        this.album_name = album_name;
    }

    public String getAlbum_image() {
        return album_image;
    }

    public void setAlbum_image(String album_image) {
        this.album_image = album_image;
    }

    public JSONObject getAlbum_songs() {
        return album_songs;
    }

    public void setAlbum_songs(JSONObject album_songs) {
        this.album_songs = album_songs;
    }


}
