package healingvisualization.androidhive.cardview;

/**
 * Created by mindgame on 3/16/2017.
 */
public class MySingleton {
    private static MySingleton ourInstance = new MySingleton();

    public static MySingleton getInstance() {
        return ourInstance;
    }

    private MySingleton() {
    }
}
