package healingvisualization.androidhive.cardview;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import java.util.ArrayList;

public class Album_listItem extends AppCompatActivity {
    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;

    String[] Title0,Title1,Desc0,Desc1;



    ArrayList<AlbumObject>arrayList=new ArrayList<>();
    int position;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album_list_item);

        Bundle b= getIntent().getExtras();
        position=b.getInt("flag");

        recyclerView= (RecyclerView) findViewById(R.id.recyclerview);
        Title0=getResources().getStringArray(R.array.titles0);
        Desc0=getResources().getStringArray(R.array.desc0);
        Title1=getResources().getStringArray(R.array.desc1);
        Desc1=getResources().getStringArray(R.array.desc1);
        if(position==0) {

            int i = 0;
            for (String name : Title0) {
                AlbumObject dataProvider = new AlbumObject(name, Desc0[i]);
                arrayList.add(dataProvider);
                i++;

            }
            adapter = new RecyclerAdapter(this,arrayList);
            recyclerView.setHasFixedSize(true);
            layoutManager = new LinearLayoutManager(this);
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.addItemDecoration(new SimpleDividerItemDecoration(this));
            recyclerView.setAdapter(adapter);

        }
        if(position==1) {

            int i = 0;
            for (String name : Title1) {
                AlbumObject dataProvider = new AlbumObject(name, Desc1[i]);
                arrayList.add(dataProvider);
                i++;

            }
            adapter = new RecyclerAdapter(this,arrayList);
            recyclerView.setHasFixedSize(true);
            layoutManager = new LinearLayoutManager(this);
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setAdapter(adapter);

        }
//
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//
//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
    }

}
