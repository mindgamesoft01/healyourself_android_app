package healingvisualization.androidhive.cardview;

import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.display.DisplayManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.PowerManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.NotificationCompat;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RemoteViews;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.ListIterator;
import java.util.concurrent.TimeUnit;
import healingvisualization.androidhive.cardview.Notifications_local;
public class AndroidDownloadFileByProgressBarActivity extends AppCompatActivity  {

    // button to show progress dialog
    Button btnDownload,my_vedioview,btn_Next,btn_Pre,btn_play, btn_pause, btn_stop,btn_back;
    int i;
    String TAG = "TAG";
    Boolean aBoolean = Boolean.TRUE;
    Boolean download_flag = Boolean.TRUE;
    Boolean PlayFlg = Boolean.FALSE, PauseFlg = Boolean.FALSE, StopFlg = Boolean.FALSE;
    Boolean mpdatasource_status = Boolean.FALSE;
    public int position = 0;
    final MediaPlayer mp = new MediaPlayer();
    long mMinDuration = 0;
    int mSecDuration = 0;
    File audio_file, audio_file_folder;
    String LOG_SEEKBAR="SEEK BAR :";
    String current_song = "";
    String file_url ="";
    // Progress Dialog
    private ProgressDialog pDialog;

    // Progress dialog type (0 - for Horizontal progress bar)
    public static final int progress_bar_type = 0;
    //google cloud bucket
    private static String bucket = "https://storage.googleapis.com/healyourself01/Healing%20Finalized%20Audios/";
    //album folder name
    private static String album_folder = "Aashiqui2(2013)";
    // audio files list to play

    TextView text_title, text_subtitle;

    ImageView image_album;
    //seekbar
    SeekBar seekBar;
    TextView seekbar_progress;
    //Teliphony manager
    TelephonyManager telephonyManager;
    PhoneStateListener listener;


    //    SET THE FILE PATH GLOBALLY
//    private static String AudioFilePath = (Environment.getExternalStorageDirectory().getPath() + "/" + file_name);
//    File audio_file = new File(Environment.getExternalStorageDirectory().getPath(), "/" + file_name);
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;
    int val;

    //NOTIFICATIONS
    private NotificationCompat.Builder builder;
    private NotificationManager notificationManager;
    private int notification_id;
    private RemoteViews remoteViews;
    Context context;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        text_title = (TextView) (findViewById(R.id.album_title));
        text_subtitle = (TextView) (findViewById(R.id.album_subtitle));
        image_album = (ImageView) findViewById(R.id.imge_album);
        btn_play = (Button) findViewById(R.id.audio_play);
        btn_Next = (Button) findViewById(R.id.audio_next);
        btn_Pre=(Button)findViewById(R.id.audio_pre);
        btn_back= (Button) findViewById(R.id.btn_back);
        /*************************INITIALIZING THE CONTEXT OBJECT***********************************************/
        context=this.getApplicationContext();
        /*************************INITIALIZING THE REMOTE VIEW FOR NOTIFICATION***********************************************/
        notificationManager=(NotificationManager)getSystemService(NOTIFICATION_SERVICE);
        remoteViews=new RemoteViews(getPackageName(),R.layout.custom_notification);
        remoteViews.setImageViewResource(R.id.notif_icon,R.drawable.promo1);
        remoteViews.setTextViewText(R.id.notif_title,"HealYourSelf");
        notification_id= (int) System.currentTimeMillis();
        Intent button_play=new Intent("playbtn_clicked");
        button_play.putExtra("id",notification_id);
        PendingIntent pendingIntent=PendingIntent.getBroadcast(context,123,button_play,0);
        remoteViews.setOnClickPendingIntent(R.id.noti_play,pendingIntent);

        /*Initialize the songs iterator*/
        final ArrayList songs = new ArrayList();


        songs.add("Song_01.mp3");
        songs.add("Song_02.mp3");
        songs.add("Song_03.mp3");
        songs.add("Song_04.mp3");
        songs.add("Song_05.mp3");
        songs.add("Song_06.mp3");
        songs.add("Song_07.mp3");
        songs.add("Song_08.mp3");
        songs.add("Song_09.mp3");
        songs.add("Song_10.mp3");
        final ListIterator  songs_iterator = songs.listIterator();

        //set the max value for seek bar to zero initially so that seek bar is disabled initially.
        //seekbar method calling
        seekbar();

        /*------------------FIREBASE DATABASE SETUP----------------------------------*/
            firebase_database fb = new firebase_database();
            fb.writetodb();
            fb.readfromdb();
        /*------------------FIREBASE DATABASE SETUP----------------------------------*/


        //call state change listener
        telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        listener = new PhoneStateListener() {
            @Override
            public void onCallStateChanged(int state, String incomingNumber) {

                switch (state) {
                    case TelephonyManager.CALL_STATE_IDLE:
                        Log.d(TAG,"idle");
                        //Play if it is paused
                        if ( mp!=null && (!mp.isPlaying()) && (PlayFlg == Boolean.FALSE) && (PauseFlg == Boolean.TRUE) ){
                            btn_play.setBackgroundResource(R.drawable.video_player_btn_pause_p);
                            btn_play.requestFocus();
                            mp.start();
                            //Set the Flags -Playing
                            PlayFlg = Boolean.TRUE;
                            PauseFlg = Boolean.FALSE;
                            StopFlg = Boolean.FALSE;
                            return;
                        }

                        break;
                    case TelephonyManager.CALL_STATE_OFFHOOK:
                        //Pause if it is playing
                        if ( mp!=null && (mp.isPlaying()) && (PlayFlg == Boolean.TRUE) && (PauseFlg == Boolean.FALSE) ) {
                            btn_play.setBackgroundResource(R.drawable.video_player_btn_pause_p);
                            btn_play.requestFocus();
                            mp.pause();
                            //Set the Flags -Pause
                            PlayFlg = Boolean.FALSE;
                            PauseFlg = Boolean.TRUE;
                            StopFlg = Boolean.FALSE;
                            return;
                        }
                        Log.d(TAG,"Off Hook");
                        break;
                    case TelephonyManager.CALL_STATE_RINGING:
                        //Pause if it is playing
                        Log.d(TAG,"Ringing");
                        //Pause if it is playing
                        if ((mp.isPlaying()) && (PlayFlg == Boolean.TRUE) && (PauseFlg == Boolean.FALSE) ) {
                            btn_play.setBackgroundResource(R.drawable.video_player_btn_pause_p);
                            btn_play.requestFocus();
                            mp.pause();
                            //Set the Flags
                            PlayFlg = Boolean.FALSE;
                            PauseFlg = Boolean.TRUE;
                            StopFlg = Boolean.FALSE;
                            return;
                        }

                        break;
                }
         }
        };
// Register the listener with the telephony manager
        telephonyManager.listen(listener, PhoneStateListener.LISTEN_CALL_STATE);

/*******************PLAY/PAUSE*****************************************/
        btn_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!mp.isPlaying()) {
                    btn_play.setBackgroundResource(R.drawable.video_player_btn_pause_p);
                    btn_play.requestFocus();
                    //load the song only if the mp object is null
                    if (songs_iterator.hasNext() && mpdatasource_status == Boolean.FALSE) {
                        mpdatasource_status = Boolean.FALSE;
                        audioplay(songs_iterator.next().toString(), album_folder, true);
                        mpdatasource_status = Boolean.TRUE;
                    }else {
                        Log.e("err","No Songs to play in songs arraylist!");
                    }
                    mp.start();
                    //Update the seekbar once the song starts playing
                    getSeekBarStatus();
                    //Set the Flags
                    PlayFlg = Boolean.TRUE;
                    PauseFlg = Boolean.FALSE;
                    StopFlg = Boolean.FALSE;
                    return;
                }

                if (mp.isPlaying()) {
                    btn_play.setBackgroundResource(R.drawable.video_player_btn_play_p);
                    btn_play.requestFocus();
                    mp.pause();
                    //Set the Flags
                    PlayFlg = Boolean.FALSE;
                    PauseFlg = Boolean.TRUE;
                    StopFlg = Boolean.FALSE;
                    return;
                }


            }
        });
        /*****************************LOCK SCREEN NOTIFICATION************************************/

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            Intent notification_intent=new Intent(context,AndroidDownloadFileByProgressBarActivity.class);
                PendingIntent pendingIntent1=PendingIntent.getActivity(context,0,notification_intent,0);
                builder =new NotificationCompat.Builder(context);
                builder.setSmallIcon(R.drawable.promo1);
                builder.setAutoCancel(true);
                builder.setCustomContentView(remoteViews);
                builder.setContentIntent(pendingIntent1);
                builder.setPriority(Notification.PRIORITY_MAX);
                notificationManager.notify(notification_id,builder.build());


//                int icon = R.drawable.gallery;
//                long when = System.currentTimeMillis();
//                Notification notify = new Notification(icon, "custom notification", when);
//                Notification notification = new Notification(icon, "Custom Notification", when);
//
//                NotificationManager mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
//
//                RemoteViews contentView = new RemoteViews(getPackageName(), R.layout.custom_notification);
//                contentView.setImageViewResource(R.id.image, R.drawable.promo1);
//                contentView.setTextViewText(R.id.title, "Custom notification");
//                contentView.setTextViewText(R.id.text, "This is a custom layout");
//                notification.contentView = contentView;
//
//                Intent notificationIntent = new Intent();
//                PendingIntent contentIntent = PendingIntent.getActivity(AndroidDownloadFileByProgressBarActivity.this, 0, notificationIntent, 0);
//                notification.contentIntent = contentIntent;
//
//                notification.flags |= Notification.FLAG_AUTO_CANCEL; //Do not clear the notification
//                notification.defaults |= Notification.DEFAULT_LIGHTS; // LED
//                notification.defaults |= Notification.DEFAULT_VIBRATE; //Vibration
//                notification.defaults |= Notification.DEFAULT_SOUND; // Sound
//
//                mNotificationManager.notify(1, notification);
            }




//                Notification.Builder notify;
//                NotificationManager nm;
//                notify=new Notification.Builder(context);
//                notify.setTicker("HealYourself");
//                notify.setWhen(System.currentTimeMillis());
//                notify .setPriority(Notification.PRIORITY_MAX);
//                notify .setContentTitle("Content Title");
//                notify .setContentText("Notification Content");
//                notify .setSmallIcon(R.drawable.promo1);
//                nm=(NotificationManager)context.getSystemService(NOTIFICATION_SERVICE);
//
//
//                Intent intent=new Intent();
//                PendingIntent pendingIntent=PendingIntent.getActivity(context,0,intent,0);
//                notify.addAction(R.drawable.video_player_btn_next_p,"",pendingIntent);
//                nm.notify(0,notify.getNotification());
//
//
//            };


        });

/***************************NEXT/PREV******************************************/
btn_Next.setOnClickListener( new View.OnClickListener() {
                                 @Override
                                 public void onClick(View v) {
                                     if (songs_iterator.hasNext()) {
                                         mp.reset();
                                         audioplay(songs_iterator.next().toString(), album_folder, true);
                                     }else {
                                         Toast.makeText(getApplicationContext(),"Reached the last song",Toast.LENGTH_SHORT).show();
                                         Log.e("err","No Songs to play in songs arraylist!");
                                     }
                                     mp.start();
                                  }
                                 }

);

btn_Pre.setOnClickListener( new View.OnClickListener() {
                                 @Override
                                 public void onClick(View v) {
                                     if (songs_iterator.hasPrevious()) {
                                         mp.reset();
                                         audioplay(songs_iterator.previous().toString(), album_folder, true);
                                     }else {
                                         Toast.makeText(getApplicationContext(),"Reached the first song",Toast.LENGTH_SHORT).show();
                                         Log.e("err","No Songs to play in songs arraylist!");
                                     }
                                     mp.start();
                                 }
                                 }

);


        //onclick listener - next button
/************************Media Player Async callback**********************************/
        //mp3 will be started after completion of preparing...
        mp.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

            @Override
            public void onPrepared(MediaPlayer player) {
                player.start();
                //calculate the duration of audio file
                if (mp!=null && mp.getDuration() >0) {
                    mMinDuration = TimeUnit.MILLISECONDS.toMinutes(mp.getDuration());
                    mSecDuration = ((mp.getDuration() / 1000) % 60);
                    mpdatasource_status =Boolean.TRUE;
                //set the max value for the seek bar
                    seekBar.setMax(mp.getDuration());
                    Log.d("", "Duration:" + mp.getDuration()+" "+mMinDuration+" "+mSecDuration);
                }

            }

        });
/************************Media Player Async callback**********************************/

        Bundle b = getIntent().getExtras();
        position = b.getInt("flag");
//        Toast.makeText(getApplicationContext(),"val:"+position,Toast.LENGTH_SHORT).show();
        if (position == 0) {
            text_title.setText(R.string.album1_title);
            text_subtitle.setText(R.string.album1_subtitle);

            Bitmap icon = BitmapFactory.decodeResource(getResources(), R.drawable.heal_album1);
            image_album.setImageBitmap(icon);
        }
        if (position == 1) {
            text_title.setText(R.string.album2_title);
            text_subtitle.setText(R.string.album2_title);

            Bitmap icon = BitmapFactory.decodeResource(getResources(), R.drawable.gallery1);
            image_album.setImageBitmap(icon);

        }
        if (position == 2) {
            text_title.setText(R.string.album3_title);
            text_subtitle.setText(R.string.album3_title);

            image_album.setBackgroundResource(R.drawable.gallery1);
        }


        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    /*********************seek bar implementation***************************/

    public void seekbar(){
         seekBar=(SeekBar)findViewById(R.id.seekbar);
         seekbar_progress=(TextView)findViewById(R.id.seekbar_progress);

        //SET Seekbar to 0 initially
        seekBar.setMax(0);
//        if (mp!=null && mp.getDuration() >0 && seekBar.getMax() == 0) {
//            int duration = mp.getDuration();
//            seekBar.setMax(duration);
//        }else {
//            seekBar.setMax(0);
//        }

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
        {
            int progress_value=0;
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progress_value=progress;
                //set seek bar max dur
                if (seekBar.getMax() <= 0 && mp.getDuration() >0) {
                    seekBar.setMax(mp.getDuration());
                }
                Log.d(LOG_SEEKBAR,"ProgressChanged");
                final long mMinutes=(progress/1000)/60;//converting into minutes
                final int mSeconds=((progress/1000)%60);//converting into seconds
                seekbar_progress.setText(mMinutes+":"+mSeconds+"/"+mMinDuration+":"+mSecDuration);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                                Log.d(LOG_SEEKBAR,"StartTracking");

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                Log.d(LOG_SEEKBAR,"StopTracking");
                /*when the pointer released, set the seek pos in player*/
                int seekToPosition = seekBar.getProgress();
                if(seekToPosition<=mp.getDuration()){
                    mp.seekTo(seekToPosition);
                }


            }
        });
    }
    public void getSeekBarStatus(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                //mp is your media player
                //progress is your progressBar
                int currentPosition=0;
                int total = mp.getDuration();
                currentPosition = mp.getCurrentPosition();
//                seekBar.setMax(total);

                while (mp !=null && total >0 && currentPosition<=total){
                    try{
                        Thread.sleep(1000);
                        currentPosition=mp.getCurrentPosition();
                    }catch (Exception e){
                        Log.d("Exception: ","exception");
                        e.printStackTrace();
                    }
                    seekBar.setProgress(currentPosition);
                }
            }
        }).start();

    }

    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case progress_bar_type:
                pDialog = new ProgressDialog(this);
                pDialog.setMessage("Downloading file. Please wait...");
                pDialog.setIndeterminate(false);
                pDialog.setMax(100);
                pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                pDialog.setCancelable(true);
                pDialog.show();
                return pDialog;
            default:
                return null;
        }
    }



    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("AndroidDownloadFileByProgressBar Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    /**************************----Audio File Download-----------**********************/
    /*   download the file with a dialog box                                                                            */
    /*   input: URL, Output File Path                                                                            */
    /*                                                                               */
    /**************************----Audio File Download-----------**********************/

    class DownloadFileFromURL  extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread
         * Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog(progress_bar_type);
        }

        /**
         * Downloading file in background thread
         */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                URL url = new URL(f_url[0]);
                Log.d("msg",url.getPath());
                String download_folder_path = f_url[1];
                URLConnection conection = url.openConnection();
                conection.connect();
                // getting file length
                int lenghtOfFile = conection.getContentLength();

                // input stream to read file - with 8k buffer
                InputStream input = new BufferedInputStream(url.openStream(), 8192);

                // Output stream to write file
                Log.d("msg",download_folder_path);
                OutputStream output = new FileOutputStream(download_folder_path);

                byte data[] = new byte[2048];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));
                    // writing data to file
                   output.write(data, 0, count);
                }
              // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();


            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            pDialog.setProgress(Integer.parseInt(progress[0]));
        }

        /**
         * After completing background task
         * Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
            dismissDialog(progress_bar_type);
            try {
                if (audio_file.exists()) {
                    Log.d("file found:", audio_file.getPath());
                    mp.setDataSource(audio_file.getPath());
                    mp.prepareAsync();
                }
                else    {
                    Log.e("Error", "Unable to download file:"+file_url+"into:"+audio_file.getPath());
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            // Displaying downloaded image into image view
            // Reading image path from sdcard
        }


    }
    /**************************----Audio File Download-----------**********************/

    /********************************Audio Play function********************************************/
    /*Desc: Downloads the file if it is not available and then prepares the player with the file*/
    /*Parm:file Name,album_name,download-yes-or-no-flag*/
    /********************************Audio Play function********************************************/
    public Boolean audioplay(String file_name, String album_folder, boolean   DownloadFlag) {
        try {

            //you can change the path, here path is external directory(e.g. sdcard) /Music/maine.mp3
            audio_file_folder = new File(Environment.getExternalStorageDirectory().getPath(), "/" +"HealYourSelf"+"/"+album_folder);
            audio_file = new File(Environment.getExternalStorageDirectory().getPath(), "/" +"HealYourSelf"+"/"+album_folder+"/"+ file_name);

            //1.Create album folder if it doesnt exist
            boolean folder_creation_flag = false;
            if (!audio_file_folder.exists()){
                folder_creation_flag = audio_file_folder.mkdirs();
            }

            //2.Check whether file exists, if not download the file
            if ( audio_file_folder.exists() && audio_file.exists()) {
                Log.d("file found:", audio_file.getPath());
//                    audio_file.delete(); //TESTING ONLY DELETE
                mp.setDataSource(audio_file.getPath());
                mp.prepareAsync();

            //3.Download the file to the local drive
            } else if (DownloadFlag) {
                file_url = bucket+album_folder+"/"+file_name;
                Log.d("msg","url sent:"+file_url);
                new DownloadFileFromURL().execute(file_url,audio_file.getPath());
            }

        } catch (Exception e) {
                e.printStackTrace();

        }
        return aBoolean;
    }

    @Override
    protected void onStart() {
        super.onStart();// ATTENTION: This was auto-generated to implement the App Indexing API.
// See https://g.co/AppIndexing/AndroidStudio for more information.
//        client.connect();
        Log.i(TAG, "---------------------------onStart-----------------");
        printstate("NONE");
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
//        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    protected void onResume() {
        super.onResume();
//        audioplay(false);
//        mp.start();
        Log.i(TAG, "------------------onResume--------------");
        printstate("NONE");
    }

    @Override
    protected void onPause() {
        super.onPause();
//        if (isScreenOn(getApplicationContext())) {
////            audioplay(false);
//            if (!StopFlg) {
//                mp.pause();
//            }
//            Log.i(TAG, "------------------onPause-----------------");
//            printstate("PAUSE");
//        }
    }

    @Override
    protected void onStop() {
        super.onStop();// ATTENTION: This was auto-generated to implement the App Indexing API.
// See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());

//        if (!isScreenOn(getApplicationContext()) && !PauseFlg && !StopFlg && PlayFlg) {
//
//            mp.start();
//        }
//        Log.i(TAG, "--------------onStop-------------------");
//
//        // ATTENTION: This was auto-generated to implement the App Indexing API.
//        // See https://g.co/AppIndexing/AndroidStudio for more information.
//        client.disconnect();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i(TAG, "--------------------------onRestart------------------------");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // Destroy the player when activity is destroyed
        mp.release();

        Log.i(TAG, "----------------------onDestroy-------------------");

    }

    /**
     * Is the screen of the device on.
     *
     * @param context the context
     * @return true when (at least one) screen is on
     */
    public boolean isScreenOn(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
            DisplayManager dm = (DisplayManager) context.getSystemService(Context.DISPLAY_SERVICE);
            boolean screenOn = false;
            for (Display display : dm.getDisplays()) {
                if (display.getState() != Display.STATE_OFF) {
                    screenOn = true;
                }
            }
            return screenOn;
        } else {
            PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            //noinspection deprecation
            return pm.isScreenOn();
        }
    }

    public boolean printstate(String action) {
        if (mp.isPlaying()) {
            Log.d(TAG, "isPlaying:TRUE");
        } else {
            Log.d(TAG, "isPlaying:FALSE");
        }

        if (isScreenOn(getApplicationContext())) {
            Log.d(TAG, "Screen:ON");
        } else {
            Log.d(TAG, "Screen:OFF");
        }
        Log.d(TAG, "ACTION:" + action);

        return true;
    }

}
